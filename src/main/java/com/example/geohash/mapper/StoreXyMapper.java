package com.example.geohash.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.geohash.pojo.StoreXy;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Douhy
 * @since 2022-04-06
 */
public interface StoreXyMapper extends BaseMapper<StoreXy> {

}
