package com.example.geohash;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@MapperScan("com.example.geohash.mapper")
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class GeoHashApplication {

    public static void main(String[] args) {
        SpringApplication.run(GeoHashApplication.class, args);
    }

}
