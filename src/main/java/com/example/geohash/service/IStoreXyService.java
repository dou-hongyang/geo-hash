package com.example.geohash.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.geohash.pojo.StoreXy;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Douhy
 * @since 2022-04-06
 */
public interface IStoreXyService extends IService<StoreXy> {

}
