package com.example.geohash.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.geohash.mapper.StoreXyMapper;
import com.example.geohash.pojo.StoreXy;
import com.example.geohash.service.IStoreXyService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Douhy
 * @since 2022-04-06
 */
@Service
public class StoreXyServiceImpl extends ServiceImpl<StoreXyMapper, StoreXy> implements IStoreXyService {

}
