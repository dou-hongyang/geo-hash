package com.example.geohash.controller;


import com.example.geohash.pojo.StoreXy;
import com.example.geohash.pojo.param.GeoHashAddParam;
import com.example.geohash.service.IStoreXyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisGeoCommands;
import org.springframework.web.bind.annotation.*;

import java.awt.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Douhy
 * @since 2021-12-13
 */
@RestController
@RequestMapping("/storeXy")
public class StoreXyController {

    @Autowired
    private IStoreXyService storeXyService;

    @Autowired
    private GeoHashController geoHashController;


    @GetMapping(value = "/listStoreXy", produces = "application/json;charset=UTF-8")
    public Boolean getAllStoreXyList() {
        List<StoreXy> list = storeXyService.list();

        ArrayList<RedisGeoCommands.GeoLocation<String>> geoLocations = new ArrayList<>();
        GeoHashAddParam<String> geoHashAddParam = new GeoHashAddParam<>();
        geoHashAddParam.setRk("healthStore");

        for (StoreXy storeXy : list) {
            String storeName = storeXy.getStoreName();
            BigDecimal longitude = storeXy.getLongitude();
            BigDecimal latitude = storeXy.getLatitude();
            RedisGeoCommands.GeoLocation<String> stringGeoLocation = new RedisGeoCommands.GeoLocation<String>(storeName,new org.springframework.data.geo.Point(longitude.doubleValue(),latitude.doubleValue()));
            geoLocations.add(stringGeoLocation);
        }
        geoHashAddParam.setGeoLocations(geoLocations);


        System.out.println(geoLocations.size());
        geoHashController.add(geoHashAddParam);

        return true;
    }

}
