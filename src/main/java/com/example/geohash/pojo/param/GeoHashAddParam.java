package com.example.geohash.pojo.param;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.redis.connection.RedisGeoCommands;

import java.util.List;

/**
 * @Author douhy
 * @description 新增入参
 * @date 22/03/31
 */
@Getter
@Setter
public class GeoHashAddParam<T> {

    // redisKey
    private String rk;

    // redisTemplate 封装对象
    private List<RedisGeoCommands.GeoLocation<T>> geoLocations;
}
