package com.example.geohash.pojo.enums;

import org.springframework.data.geo.Metric;

// 距离单位
public enum DistanceUnitEnum implements Metric {

    METERS(6378137.0D, "m",1),
    KILOMETERS(6378.137D, "km",2),
    MILES(3963.191D, "mi",3),
    FEET(2.0925646325E7D, "ft",4);

    private double multiplier;
    private String abbreviation;
    private Integer unit;


    DistanceUnitEnum(double multiplier, String abbreviation, Integer unit) {
        this.multiplier = multiplier;
        this.abbreviation = abbreviation;
        this.unit = unit;
    }

    public double getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(double multiplier) {
        this.multiplier = multiplier;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public Integer getUnit() {
        return unit;
    }

    public void setUnit(Integer unit) {
        this.unit = unit;
    }

    // 获取业务枚举
    public static DistanceUnitEnum getEnum(Integer unit){
        for (DistanceUnitEnum distanceUnitEnum : DistanceUnitEnum.values()) {
            if (unit == distanceUnitEnum.getUnit()){
                return distanceUnitEnum;
            }
        }
        return null;
    }

}
